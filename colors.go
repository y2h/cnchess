package main

import "image/color"

var (
	red = color.RGBA{255, 0, 0, 255}
	green = color.RGBA{0, 255, 0, 255}
	blue = color.RGBA{0, 0, 255, 255}
	bgc = color.RGBA{255, 228, 225, 255}
	sc  = color.RGBA{150, 205, 205, 255}
)
