package main

import (
	"image/color"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/widget"
)

var _ fyne.Widget = (*Board)(nil)

type Board struct {
	widget.BaseWidget
	// 网格数
	N int
	// 背景、外框粗、楚河 汉界
	Bg, outer, center *canvas.Rectangle
	// 横线、竖线、中间4条斜线
	linesV, linesH, linesCenter []*canvas.Line
	// 楚河 汉界
	chhj     *canvas.Text
	r        *boardRenderer
	objs     []fyne.CanvasObject
	OnTapped func(*Board) `json:"-"`
}

func NewBoard(n int) *Board {
	s := &Board{}
	s.Bg = canvas.NewRectangle(bgc)
	c := color.Black
	s.chhj = canvas.NewText("楚河                汉界", c)
	s.chhj.Alignment = fyne.TextAlignCenter
	s.center = canvas.NewRectangle(bgc)
	s.center.CornerRadius = 0
	s.center.StrokeWidth = 1
	s.center.StrokeColor = c
	s.N = n

	s.outer = canvas.NewRectangle(nil)
	s.outer.StrokeWidth = 3
	s.outer.StrokeColor = c
	for i := 0; i < s.N-1; i++ {
		s.linesV = append(s.linesV, canvas.NewLine(c))
	}
	for i := 0; i < s.N; i++ {
		s.linesH = append(s.linesH, canvas.NewLine(c))
	}
	for i := 0; i < 4; i++ {
		s.linesCenter = append(s.linesCenter, canvas.NewLine(c))
	}
	s.ExtendBaseWidget(s)
	return s
}

func (s *Board) Reset() *Board {
	s.r.Layout(s.Size())
	return s
}
func (me *Board) Tapped(e *fyne.PointEvent) {
	// if me.curqz != nil {
	// 	me.curqz.Move(e.Position.SubtractXY(me.curqz.r/2, me.curqz.r/2))
	// }
	if me.OnTapped != nil {
		me.OnTapped(me)
	}
}

//	func (me *Board) Cursor() desktop.Cursor {
//		return desktop.PointerCursor
//	}
func (s *Board) MinSize() fyne.Size {
	return fyne.NewSquareSize(12 * 40)
}

func (s *Board) CreateRenderer() fyne.WidgetRenderer {
	s.ExtendBaseWidget(s)
	objs := []fyne.CanvasObject{}
	objs = append(objs, s.Bg)
	for _, v := range s.linesH {
		objs = append(objs, v)
	}
	for _, v := range s.linesV {
		objs = append(objs, v)
	}
	for _, v := range s.linesCenter {
		objs = append(objs, v)
	}
	objs = append(objs, s.outer)
	objs = append(objs, s.center)
	objs = append(objs, s.chhj)
	s.objs = objs
	s.r = &boardRenderer{
		d: s,
	}

	return s.r
}

var _ fyne.WidgetRenderer = (*boardRenderer)(nil)

type boardRenderer struct {
	d *Board
}

func (r *boardRenderer) Objects() []fyne.CanvasObject {
	return r.d.objs
}

func (r *boardRenderer) Layout(size fyne.Size) {
	r.d.Bg.Resize(size)
	var s float32 = 0.0
	if size.Width > size.Height {
		s = size.Height
	} else {
		s = size.Width
	}
	s -= 40
	var offset float32 = 20
	si := (s) / float32(r.d.N-1)
	r.d.outer.Resize(fyne.NewSize(s, s+si))
	r.d.center.Resize(fyne.NewSize(8*si, si*1.0))
	r.d.center.Move(fyne.NewPos(offset, si*4+offset))
	r.d.chhj.Resize(r.d.center.Size())
	r.d.chhj.Move(r.d.center.Position())
	// 横线
	for i, l := range r.d.linesH {
		layoutLine(offset, float32(i)*si+offset, s-si+offset, float32(i)*si+offset, l)
	}
	// 竖线
	for i, l := range r.d.linesV {
		layoutLine(float32(i)*si+offset, offset, float32(i)*si+offset, s+offset, l)
	}

	layoutLine(3*si+offset, offset, 5*si+offset, 2*si+offset, r.d.linesCenter[0])
	layoutLine(5*si+offset, offset, 3*si+offset, 2*si+offset, r.d.linesCenter[1])
	layoutLine(3*si+offset, 7*si+offset, 5*si+offset, 9*si+offset, r.d.linesCenter[2])
	layoutLine(3*si+offset, 9*si+offset, 5*si+offset, 7*si+offset, r.d.linesCenter[3])
}
func (r *boardRenderer) MinSize() fyne.Size {
	return fyne.NewSquareSize(10)
}

func (r *boardRenderer) Refresh() {

}
func (r *boardRenderer) Destroy() {
}
