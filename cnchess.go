package main

import (
	"image/color"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

var _ fyne.Widget = (*CnChess)(nil)

type CnChess struct {
	widget.BaseWidget
	// 网格数
	N int
	// 当前棋子的坐标
	curr, curc int
	// hovered                     bool
	ss [][]int

	Btns []*Qizi
	// 红黑棋子，被吃掉的红r黑b棋子
	qzblack, qzred, ateb, ater []*Qizi
	// 当前棋子
	curqz *Qizi
	// 轮到哪一方下棋，1 红方，2 黑方
	turn     int
	r        *cnChessRenderer
	objs     []fyne.CanvasObject
	OnTapped func(*CnChess) `json:"-"`
}

func NewCnChess(n int, w fyne.Window, fn func(*CnChess)) *CnChess {
	s := &CnChess{}
	s.turn = 1
	s.OnTapped = fn
	s.N = n

	s.qzblack = make([]*Qizi, len(qizir))
	s.qzred = make([]*Qizi, len(qizir))
	for i, v := range qizib {
		s.qzblack[i] = NewQizi(v, func(q *Qizi) {
			s.qzclicked(q, 1)
		}).Do(func(q *Qizi) {
			q.Data1 = 2
		})
		s.qzblack[i].Text.Color = color.Black
		s.qzred[i] = NewQizi(qizir[i], func(q *Qizi) {
			s.qzclicked(q, 2)
		}).Do(func(q *Qizi) {
			q.Data1 = 1
		})
	}

	// b := false
	s.ss = make([][]int, s.N)
	for i := 0; i < n; i++ {
		// i0:=i
		for j := 0; j < n-1; j++ {
			// j0:=j
			s.ss[i] = append(s.ss[i], 0)
			s.Btns = append(s.Btns, NewQizi("", nil).Do(func(q *Qizi) {
				q.HoverColor = green
				q.NormalColor = bgc
				q.Image.FillColor = nil
				q.Image.StrokeColor = bgc
				q.Image.StrokeWidth = 0.5
				q.OnTapped = func(q *Qizi) {
					if s.curqz != nil {
						if s.turn == s.curqz.Data1 {
							s.curqz.Move(q.Position())
							s.curqz.pos=q.Position()
							s.curqz = nil
							s.turned()
						}
					}
				}
			}))
		}
	}
	s.ExtendBaseWidget(s)
	return s
}

// 棋子点击事件
func (s *CnChess) qzclicked(q *Qizi, hs int) {
	if s.curqz != nil && s.curqz.Data1 == hs {
		// 被吃掉
		s.objs = deleteObj(s.objs, q)
		if hs == 1 {
			s.ateb = append(s.ateb, q)
		} else if hs == 2 {
			s.ater = append(s.ater, q)
		}
		s.curqz.pos = q.Position()
		s.curqz = nil
		s.turned()
		s.r.Layout(s.Size())
		return
	}
	if s.turn == hs {
		return
	}
	if s.curqz == nil || s.curqz.Data1 == q.Data1 {
		s.curqz = q
		return
	}
}

// 红、黑轮流下棋
func (s *CnChess) turned() *CnChess {
	if s.turn == 1 {
		s.turn = 2
	} else if s.turn == 2 {
		s.turn = 1
	}
	return s
}

// 重置棋子
func (s *CnChess) Reset() *CnChess {
	for i := 0; i < s.N; i++ {
		for j := 0; j < s.N-1; j++ {
			s.ss[i][j] = 0
		}
	}
	s.turn = 1
	s.curqz = nil
	for _, v := range s.ater {
		s.objs = append(s.objs, v)
	}
	for _, v := range s.ateb {
		s.objs = append(s.objs, v)
	}
	for i, v := range s.qzblack {
		v.pos.X = 0
		s.qzred[i].pos.X = 0
	}
	s.r.Layout(s.Size())
	return s
}
func (me *CnChess) Tapped(e *fyne.PointEvent) {
	// if me.curqz != nil {
	// 	me.curqz.Move(e.Position.SubtractXY(me.curqz.r/2, me.curqz.r/2))
	// }
	if me.OnTapped != nil {
		me.OnTapped(me)
	}
}

//	func (me *CnChess) Cursor() desktop.Cursor {
//		return desktop.PointerCursor
//	}
func (s *CnChess) MinSize() fyne.Size {
	return fyne.NewSquareSize(12 * 40)
}

func (s *CnChess) CreateRenderer() fyne.WidgetRenderer {
	s.ExtendBaseWidget(s)
	objs := []fyne.CanvasObject{}

	for _, v := range s.Btns {
		objs = append(objs, v)
	}
	for _, v := range s.qzblack {
		objs = append(objs, v)
	}
	for _, v := range s.qzred {
		objs = append(objs, v)
	}
	s.objs = objs
	s.r = &cnChessRenderer{
		d: s,
	}

	return s.r
}

var _ fyne.WidgetRenderer = (*cnChessRenderer)(nil)

type cnChessRenderer struct {
	d *CnChess
}

func (r *cnChessRenderer) Layout(size fyne.Size) {
	var s float32 = 0.0
	if size.Width > size.Height {
		s = size.Height
	} else {
		s = size.Width
	}
	s -= 40
	var offset float32 = 20
	si := (s) / float32(r.d.N-1)
	// 棋子大小
	qs := fyne.NewSquareSize(si - 5)
	for i, v := range r.d.qzblack {
		v.Resize(qs)
		r.d.qzred[i].Resize(qs)
	}

	var scale float32 = 0.4
	for i := 0; i < r.d.N; i++ {
		for j := 0; j < r.d.N-1; j++ {
			r.d.Btns[i*(r.d.N-1)+j].Resize(qs)
			r.d.Btns[i*(r.d.N-1)+j].Move(fyne.NewPos(float32(j)*si+offset, float32(i)*si+20).SubtractXY(si*scale, si*scale))
		}
	}
	for i := 0; i < 9; i++ {
		if r.d.qzblack[i].pos.X != 0 {
			r.d.qzblack[i].Move(r.d.qzblack[i].pos)
		} else {
			r.d.qzblack[i].Move(fyne.NewPos(offset+si*float32(i), offset).SubtractXY(si*scale, si*scale))
		}

		if r.d.qzred[i].pos.X != 0 {
			r.d.qzred[i].Move(r.d.qzred[i].pos)
		} else {
			r.d.qzred[i].Move(fyne.NewPos(offset+si*float32(i), offset+si*9).SubtractXY(si*scale, si*scale))
		}
	}
	// 兵、卒
	for i, v := range r.d.qzblack[11:] {
		if v.pos.X != 0 {
			v.Move(v.pos)
		} else {
			v.Move(fyne.NewPos(offset+si*float32(i)*2, offset+si*float32(3)).SubtractXY(si*scale, si*scale))
		}
	}
	for i, v := range r.d.qzred[11:] {
		if v.pos.X != 0 {
			v.Move(v.pos)
		} else {
			v.Move(fyne.NewPos(offset+si*float32(i)*2, offset+si*float32(6)).SubtractXY(si*scale, si*scale))
		}
	}
	// 炮
	if r.d.qzblack[9].pos.X != 0 {
		r.d.qzblack[9].Move(r.d.qzblack[9].pos)
	} else {
		r.d.qzblack[9].Move(fyne.NewPos(offset+si*1, offset+si*2).SubtractXY(si*scale, si*scale))
	}
	if r.d.qzblack[10].pos.X != 0 {
		r.d.qzblack[10].Move(r.d.qzblack[10].pos)
	} else {
		r.d.qzblack[10].Move(fyne.NewPos(offset+si*7, offset+si*2).SubtractXY(si*scale, si*scale))
	}
	if r.d.qzred[9].pos.X != 0 {
		r.d.qzred[9].Move(r.d.qzred[9].pos)
	} else {
		r.d.qzred[9].Move(fyne.NewPos(offset+si*1, offset+si*7).SubtractXY(si*scale, si*scale))
	}
	if r.d.qzred[10].pos.X != 0 {
		r.d.qzred[10].Move(r.d.qzred[10].pos)
	} else {
		r.d.qzred[10].Move(fyne.NewPos(offset+si*7, offset+si*7).SubtractXY(si*scale, si*scale))
	}
}

func (r *cnChessRenderer) Objects() []fyne.CanvasObject {
	return r.d.objs
}

func (r *cnChessRenderer) MinSize() fyne.Size {
	return fyne.NewSquareSize(10)
}

func (r *cnChessRenderer) Refresh() {

}
func (r *cnChessRenderer) Destroy() {
}
