package main

import (
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/theme"
	// thx "fyne.io/x/fyne/theme"
)

func main() {
	a := app.NewWithID("fyne1")
	// a.Settings().SetTheme(thx.AdwaitaTheme())
	w := a.NewWindow("go语言GUI框架fyne教程")
	w.SetIcon(theme.FyneLogo())
	cnchess.ui(w)
	w.CenterOnScreen()
	w.ShowAndRun()
}
