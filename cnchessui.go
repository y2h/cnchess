package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

var cnchess = _cnchess{}

type _cnchess struct{}

func (me _cnchess) ui(w fyne.Window) {
	lblMsg := widget.NewLabel("label")
	lblMsg.Alignment = fyne.TextAlignCenter
	g := NewCnChess(10, w, nil)
	bg := NewBoard(10)
	btnRefresh := widget.NewButtonWithIcon("",
		theme.ViewRefreshIcon(), func() {
			g.Reset()
		})

	line1 := canvas.NewLine(red)
	line1.Resize(fyne.NewSquareSize(50))
	line1.Move(fyne.NewSquareOffsetPos(150))
	line2:=NewLine(50,50,30,90)
	left := container.NewWithoutLayout(line1,line2)
	btnRotate := widget.NewButton("Rotate", func() {
		// RotateLine21(line1, -0.2)
		RotateLine(line2,10)
	})
	c := container.NewBorder(container.NewHBox(lblMsg, btnRefresh, btnRotate),
		nil, nil, nil, bg, g,left)

	w.SetContent(c)
	w.SetTitle("五子棋Gomoku")
	w.Resize(fyne.NewSize(12*40, 14*40))
	w.SetContent(c)
}
